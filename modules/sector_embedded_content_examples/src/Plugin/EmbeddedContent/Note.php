<?php

namespace Drupal\sector_embedded_content_examples\Plugin\EmbeddedContent;

use Drupal\embedded_content\EmbeddedContentInterface;
use Drupal\embedded_content\EmbeddedContentPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin note.
 *
 * @EmbeddedContent(
 *   id = "note",
 *   label = @Translation("Note"),
 *   description = @Translation("Renders a Note component."),
 * )
 */
class Note extends EmbeddedContentPluginBase implements EmbeddedContentInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'heading' => NULL,
      'message' => NULL,
      'type' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'note',
      '#heading' => $this->configuration['heading'],
      '#message' =>  $this->configuration['message']['value'] ?? NULL,
      '#type' => $this->configuration['type']
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#default_value' => $this->configuration['heading'],
      '#description' => $this->t('Renders inside a h4 element.'),
    ];

    $form['message'] = [
      '#type' => 'text_format',
      '#format' => 'sector_restricted_basic_html',
      '#title' => $this->t('Message'),
      '#default_value' => $this->configuration['message']['value'] ?? NULL,
      '#required' => TRUE,
      '#rows' => 3,
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->configuration['type'] ?? 'note--default',
      '#required' => TRUE,
      '#options' => [
        'note--default' => $this->t('Default (black)'),
        'note--alert' => $this->t('Alert (yellow)'),
        'note--info' => $this->t('Info (blue)'),
        'note--danger' => $this->t('Danger (red)'),
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $body = check_markup($form_state->getValue('message'), 'sector_restricted_basic_html');

    $this->configuration['message'] = $body;
  }

}
